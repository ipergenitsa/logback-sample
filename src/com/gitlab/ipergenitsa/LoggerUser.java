package com.gitlab.ipergenitsa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerUser {
    private static final Logger LOG = LoggerFactory.getLogger(LoggerUser.class);

    public void sayTooDetailedToLog(String message) {
        LOG.trace("This is too detailed log: {}", message); // here we use as a parameter
    }

    public static void saySomethingFromStaticContext(String message) {
        LOG.info("From static context: {}", message); // also a parameter
    }

    public void sayDebug(String message) {
        LOG.debug("From debug " + message); // here we use it as String concatenation
    }

    public void sayError(String errorMessage) {
        LOG.error("From error " + errorMessage);
    }

    public void sayWarn(String warn) {
        LOG.warn("Warning message: {}", warn);
    }

    public void sayException(String exceptionMessage) {
        try {
            throw new RuntimeException(exceptionMessage);
        } catch (Exception e) {
            LOG.warn("An exception raised", e);
        }
    }
}
