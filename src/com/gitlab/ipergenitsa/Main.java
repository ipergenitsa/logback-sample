package com.gitlab.ipergenitsa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            LoggerUser.saySomethingFromStaticContext("I'm logged without creation of an object");
            LoggerUser loggerUser = new LoggerUser();
            loggerUser.sayTooDetailedToLog("...message...");
            loggerUser.sayDebug("...message...");
            loggerUser.sayError("...message...");
            loggerUser.sayWarn("...message...");
            loggerUser.sayException("this will be message of an exception");
            LOG.info("This was fun");
        }
    }
}
